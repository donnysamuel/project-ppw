from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Post_Form
from .models import Post
# Create your views here.

response = {}
def index(request):
	html = 'update_status/updateStatus.html'
	response['post_form'] = Post_Form
	banyak = Post.objects.all().count()
	post = Post.objects.order_by('-created_date')
	response['post'] = post
	return render(request, html, response)

def add_post(request):
	form = Post_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['post'] = request.POST['post']
		post = Post(post=response['post'])
		post.save()
	return HttpResponseRedirect('/update-status/')
