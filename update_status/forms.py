from django import forms

class Post_Form(forms.Form):
	error_messages = {
		'required': 'Post Tidak Boleh Kosong',
	}
	attrs = {
		'class': 'text-design',
		'placeholder':"Apa yang anda pikirkan ....",
	}
	post = forms.CharField(error_messages=error_messages, label="", required=True, max_length=1600, widget=forms.Textarea(attrs=attrs))
